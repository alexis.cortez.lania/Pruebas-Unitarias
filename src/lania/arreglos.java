/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania;

/**
 *
 * @author alexiscortez
 */
public class arreglos {
    
    public int[] calcularPersonasTiempoParaArreglos(int n){
        int tiempo = 15;
        int[] result = new int[2];
          
        if (n <= 0){
            result = null;
        }
        
        if (n >= 1 && n <= 19) {
            result[0] = 1;
            result[1] = (int)(n * tiempo);
        }
        
        if (n >= 20 && n <= 49) {
            result[0] = 2;
            result[1] = (int)((n * tiempo)/2);
        }
        
        if (n >= 50) {
            int temp = (int)(n / 25) + 1;
            result[0] = temp;
            result[1] = (int)((n * tiempo)/temp);
        }
        return result;
    }
}
