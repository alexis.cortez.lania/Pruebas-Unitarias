/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alexiscortez
 */
public class arreglosTest {
    
    public arreglosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularPersonasTiempoParaArreglos method, of class arreglos.
     */
    @Test
    public void testCalcularPersonasCero() {
        int n = 0;
        arreglos instance = new arreglos();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testCalcularPersonasConNumerosNegativos() {
        int n = -2;
        arreglos instance = new arreglos();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testCalcularPersonasConNumerosMenosde19() {
        int n = 10;
        arreglos instance = new arreglos();
        int[] expResult = {1,150};
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testCalcularPersonasConNumerosde20a49() {
        int n = 30;
        arreglos instance = new arreglos();
        int[] expResult = {2,225};
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testCalcularPersonasConNumerosde50enAdelante() {
        int n = 75;
        arreglos instance = new arreglos();
        int[] expResult = {4,281};
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testCalcularPersonasConNumeros100() {
        int n = 100;
        arreglos instance = new arreglos();
        int[] expResult = {5,300};
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
    }
    
}
